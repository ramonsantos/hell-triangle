class HellTriangle
  def max_total(triangle)
    @triangle = triangle

    return nil unless valid?

    build_paths

    max = 0

    for ps in @paths
      sum = 0

      for i in 0...@triangle.length
        sum = sum + @triangle[i][ps[i]]
      end

      if sum > max
        max = sum
      end
    end

    return max
    
  end


  private

  def valid?
    return false unless @triangle.kind_of? Array

    return false if @triangle.empty?
    for i in 0...@triangle.length
      line = @triangle[i]

      return false unless line.kind_of? Array

      return false unless line.length == (i + 1)
      line.each do |l|
        return false unless l.kind_of? Integer
      end
    end
    return true
  end


  def build_paths
    @depth = @triangle.length
    @current_level_depth = 0

    @paths = []
    @current_path = []
    @current_path[@current_level_depth] = 0

    get_paths(0)

    return @paths
  end

  def get_paths(index)
    @current_level_depth = @current_level_depth + 1

    for i in index..(index + 1)
      @current_path[@current_level_depth] = i

      if (@current_level_depth == @depth - 1)
        @paths << @current_path.clone
      else
        get_paths(i)
      end
    end

    @current_level_depth = @current_level_depth - 1
  end

end


#ttt = HellTriangle.new

#puts ttt.max_total([[6],[3,5],[9,7,1],[9,6,8,4]])

