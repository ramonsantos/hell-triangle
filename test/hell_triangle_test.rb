$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'minitest/autorun'
require 'hell_triangle'

class TriangleHellTest < Minitest::Test
  def setup
    @hell_triangle = HellTriangle.new
  end

  def test_triangle_is_empty
    assert_nil @hell_triangle.max_total([])
  end

  def test_triagle_with_different_kind
    assert_nil @hell_triangle.max_total(nil)
  end

  def test_triagle_with_different_kind_in_array
    assert_nil @hell_triangle.max_total([[1],[2, '3'], [4, 5, 6]])
  end

  def test_triagle_is_invalid
    assert_nil @hell_triangle.max_total([[1], [1]])
  end

  def test_triagle_is_valid
    assert_kind_of Integer, @hell_triangle.max_total([[6],[3,5],[9,7,1],[9,6,8,4]])
  end

 
          # Testar triangulo vazio

      
      # Testar entrada de dados diferente de array/matrix
      
      # Testar triangulo inválido
      
    # Testar triangulo com apenas 1 elemento

  # Testar triangulo com primeria linha de maior caminho
  
  # Testar triangulo com útima linha de maior caminho

  # Testar triangulo com maior caminho não ordenado
  
  #def test_max_total
  #  assert_equal 27, @hell_triangle.max_total([[6],[3,5],[9,7,1],[9,6,8,4]])
  #end

end